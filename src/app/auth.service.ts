import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { User } from './interfaces/user';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private logInErrorSubject = new Subject<string>();
  public err; 
  user: Observable<User | null>

  
  public getLoginErrors():Subject<string>{
    return this.logInErrorSubject;
  }

  getUser(){
    return this.user
  }

  
  signup(email:string, passwoerd:string){
    this.afAuth
        .createUserWithEmailAndPassword(email,passwoerd)
        .then(res =>  {
                      console.log('Succesful sign up firebase',res);
                      this.router.navigate(['/welcome']);
                    })
         .catch (
            error => this.err = error
             )  
  }

  logout(){
    this.afAuth.signOut()
    .then(
      res =>  
       {
         console.log('Succesful logout',res);
         this.router.navigate(['/welcome']);
       }     
   )
  }

  login(email:string, password:string){
    this.afAuth
    .signInWithEmailAndPassword(email,password)
        .then(
           res =>  
            {
              console.log('Succesful Login',res);
              this.router.navigate(['/welcome']);
            }     
        )
        .catch (
          error => this.err = error
           ) 
  }

  
  signupwithoutlogin(id:number,email:string, passwoerd:string){
    var config = {apiKey: "AIzaSyAGPFMU5ItMoxk4PXdP8JGQLUibfkkzGSg",
    authDomain: "final-test-c5169.firebaseapp.com",
    databaseURL: "https://final-test-c5169.firebaseio.com"};
    
    var secondaryApp = firebase.initializeApp(config, "Secondary"+id);
    
    secondaryApp.auth().createUserWithEmailAndPassword(email, passwoerd).then(function(firebaseUser) {
        console.log("User " + firebaseUser.user.uid + " created successfully!");
        //I don't know if the next statement is necessary 
        secondaryApp.auth().signOut();
    });
  }
  
  constructor(public afAuth:AngularFireAuth,private router:Router) {
    this.user = this.afAuth.authState; 
   }
}
