import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import 'firebase/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  userCollection:AngularFirestoreCollection = this.db.collection('users');
  todoCollection:AngularFirestoreCollection;


  addtodo(userId:string,todo:string){
    const todos = {todo:todo, vi:false};
    this.userCollection.doc(userId).collection('todo').add(todos);
  }

  gettodo(userId):Observable<any[]>{
    this.todoCollection = this.db.collection(`users/${userId}/todo`);
        console.log('todo collection created');
        return this.todoCollection.snapshotChanges().pipe(
          map(actions => actions.map(a => {
            const data = a.payload.doc.data();
            data.id = a.payload.doc.id;
            return { ...data };
          }))
        ); 
  }

  
  updatevi(userId:string, id:string){
    this.db.doc(`users/${userId}/todo/${id}`).update(
       {
         vi:true
       }
     )
   }

  constructor(private db:AngularFirestore) { }
}
