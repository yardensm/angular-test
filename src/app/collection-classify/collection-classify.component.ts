import { AuthService } from './../auth.service';
import { ClassifyService } from './../classify.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-collection-classify',
  templateUrl: './collection-classify.component.html',
  styleUrls: ['./collection-classify.component.css']
})
export class CollectionClassifyComponent implements OnInit {

  docs$:Observable<any>;
  userId:string;

  constructor(private classify:ClassifyService,public auth:AuthService) { }

  ngOnInit(): void {
    this.auth.user.subscribe(
      user => {
        this.userId = user.uid;
        this.docs$ = this.classify.getdocs(this.userId);
        console.log(this.docs$)
       }
    )
  }

}
