import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import 'firebase/firestore';

@Injectable({
  providedIn: 'root'
})
export class ClassifyService {
  
  private url = "https://bypmjnnc8a.execute-api.us-east-1.amazonaws.com/final"

  public categories:object = {0: 'business', 1: 'entertainment', 2: 'politics', 3: 'sport', 4: 'tech'}
  public doc:string;

  userCollection:AngularFirestoreCollection = this.db.collection('users');
  docCollection:AngularFirestoreCollection;

  
  
  addarticles(userId:string,doc:string, classify:string){
    const article = {doc:doc, classify:classify};
    this.userCollection.doc(userId).collection('doc').add(article);
  }

  classify111(psy:number,math:number,completesalary:string):Observable<number>{
    const json = {
      "data":
        {"mathematics":math,
          "psychometric": psy,
          "cash":completesalary
        }
    }
    console.log("jjjjj",json)
    let body = JSON.stringify(json);
    return this.http.post<any>(this.url,body).pipe(
      map(res =>{
        let final = res.body
        return final; 
      })
    )
  }

  classify():Observable<any>{
    let json = {
      "articles":[
        {"text":this.doc}
      ]
    }
    let body = JSON.stringify(json);
    return this.http.post<any>(this.url,body).pipe(
      map(res =>{
        let final = res.body.replace('[','')
        final = final.replace(']','')
        return final; 
      })
    )
  }

  
  getdocs(userId):Observable<any[]>{
    this.docCollection = this.db.collection(`users/${userId}/doc`);
        console.log('doc collection created');
        return this.docCollection.snapshotChanges().pipe(
          map(actions => actions.map(a => {
            const data = a.payload.doc.data();
            data.id = a.payload.doc.id;
            return { ...data };
          }))
        ); 
  }


  constructor(private http:HttpClient,private db:AngularFirestore) { }
}
