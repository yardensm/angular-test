import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { SuserService } from '../suser.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-viewuser',
  templateUrl: './viewuser.component.html',
  styleUrls: ['./viewuser.component.css']
})
export class ViewuserComponent implements OnInit {

  userId:string;
  uss$:Observable<any>;

  constructor(public auth:AuthService,public suser:SuserService) { }

  ngOnInit(): void {
    this.auth.user.subscribe(
      user => {
        this.userId = user.uid;
        this.uss$ = this.suser.getusr(this.userId);
        console.log(this.uss$)
       }
    )
  }
  

}
