import { Component, OnInit } from '@angular/core';
import { Userapi } from '../interfaces/userapi';
import { Observable } from 'rxjs';
import { SuserService } from '../suser.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-alluser',
  templateUrl: './alluser.component.html',
  styleUrls: ['./alluser.component.css']
})
export class AlluserComponent implements OnInit {

  users$:Observable<Userapi[]>;
  clickButton:boolean = false;
  userId:string;
  id = 0; 

  constructor(private userserv:SuserService, public auth:AuthService) { }

  addaccount(id:number,email:string){
    this.clickButton = true;
    this.auth.signupwithoutlogin(id,email,"12345678");
    id++
    this.userserv.addusertodb(this.userId,id,email);
  }

  ngOnInit(): void {
    this.userserv.getusers()
    .subscribe(data =>this.users$ = data );

    this.auth.user.subscribe(
      user => {
        this.userId = user.uid;
      }
    )
  }
  

}
